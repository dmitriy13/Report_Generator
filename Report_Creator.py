﻿import sys
import xlwt
import datetime


def xls_crearor(name_file, xls_report_name, post, user_name,
                notation, inclusion, incatalog, outcatalog):

    '''Принимает имя файла и создаёт xls-отчёт'''

    file_name = open(incatalog+'/'+name_file, 'r', encoding='windows-1251')

    fnt = xlwt.Font()
    fnt.name = 'Arial'
    fnt.height = 160

    fmt = 'D-MMM-YYYY'                             # формат отображения даты

    borders = xlwt.Borders()                       # сплошная тонкая граница
    borders.left = xlwt.Borders.THIN
    borders.right = xlwt.Borders.THIN
    borders.top = xlwt.Borders.THIN
    borders.bottom = xlwt.Borders.THIN

    alignment = xlwt.Alignment()                 # выравнивание по правому краю
    alignment.horz = xlwt.Alignment.HORZ_RIGHT

    pattern = xlwt.Pattern()                          # белая заливка
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = 1

    pattern1 = xlwt.Pattern()                         # бежевая заливка
    pattern1.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern1.pattern_fore_colour = 26

    pattern2 = xlwt.Pattern()                         # красная заливка
    pattern2.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern2.pattern_fore_colour = 2

    pattern3 = xlwt.Pattern()                         # салатовая заливка
    pattern3.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern3.pattern_fore_colour = 395

    pattern4 = xlwt.Pattern()                        # жёлтая заливка
    pattern4.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern4.pattern_fore_colour = 5

    style = xlwt.XFStyle()
    style.font = fnt
    style.borders = borders
    style.alignment = alignment
    style.pattern = pattern

    style1 = xlwt.XFStyle()
    style1.font = fnt
    style1.borders = borders
    style1.pattern = pattern1

    style2 = xlwt.XFStyle()
    style2.font = fnt
    style2.alignment = alignment
    style2.pattern = pattern

    style3 = xlwt.XFStyle()
    style3.font = fnt
    style3.pattern = pattern

    style4 = xlwt.XFStyle()
    style4.font = fnt
    style4.borders = borders
    style4.pattern = pattern2

    style5 = xlwt.XFStyle()
    style5.font = fnt
    style5.borders = borders
    style5.pattern = pattern3

    style6 = xlwt.XFStyle()
    style6.font = fnt
    style6.borders = borders
    style6.pattern = pattern4

    styleDate = xlwt.XFStyle()
    styleDate.num_format_str = fmt
    styleDate.font = fnt

    book = xlwt.Workbook(encoding='windows-1251')
    sheet1 = book.add_sheet('IGBT_devices')

    for i in range(0, 8):
        sheet1.col(i).set_style(style3)

    def del_items(l, index):

        ''' Удаляет ненужные для вывода в отчёт данные'''

        del l[index:]
        del l[4]
        del l[3]
        del l[2]
        del l[1]
        del l[0]

    def title_maker():

        '''создаёт заголовок'''

        sheet1.write(2, 5, datetime.datetime.now(), styleDate)
        st1 = 'Измерение статических параметров модулей IGBT'
        sheet1.write_merge(0, 0, 0, 3, st1, style3)   # Объединяет ячейки
        sheet1.write_merge(3, 3, 4, 5, name_file, style2)

        row = 2

        for string_of_file in file_name:
            l = string_of_file.rstrip('\n').split('\t')

            if len(l) == 1:
                break
            sheet1.write(row, 0, l[0], style3)
            sheet1.write_merge(row, row, 1, 3, l[1], style3)
            row += 1
        if notation != '':
            sheet1.write(5, 0, notation, style3)

        return row + 3

    def measure_maker(row):

        '''создаёт часть с параметрами измерений'''

        limits = []   # хранит списки с нижними и верхними пределами параметров

        for string_of_file in file_name:
            l = string_of_file.rstrip('\n').split('\t')

            if len(l) == 1:
                break

            if len(l[4]) > 1 and 'LIMITS' not in l[4]:  # извлекает пределы
                                                        # параметров

                min_max = l[4].split(' ')
                del min_max[3]
                del min_max[1]
                min_max[0] = float(min_max[0].replace(',', '.'))
                min_max[1] = float(min_max[1].replace(',', '.'))
                limits.append(min_max)

            sheet1.write(row, 0, 'TEST '+l[0], style3)
            sheet1.write(row, 1, l[1], style3)
            sheet1.write(row, 2, l[2], style3)
            sheet1.write_merge(row, row, 3, 4, l[3], style3)
            sheet1.write_merge(row, row, 5, 6, l[4], style3)

            row += 1

        return row+2, limits

    def constant_part_maker():

        '''выделяет постоянную часть измеряемых параметров'''

        constant_part = []     # хранит шапку для таблиц результатов измерений
        i = 1

        for string_of_file in file_name:
            l = string_of_file.split('\t')
            constant_part.append(l)

            if i == 2:
                break
            i += 1

        index = constant_part[1].index('')   # определяет начальный индекс
                                             # данных нетребующихся для
                                             # включения в отчёт

        for x in constant_part:
            del_items(x, index)

        constant_part.reverse()

        return constant_part, index

    def rerult_measurements(row, constant_part, index):

        '''Формитует результаты измерений'''

        column = 0
        for i in range(8):
            sheet1.col(i).width = 3000               # задаёт ширину колонки

        for string_of_file in file_name:  # заполняет отчёт результатами измерений
            l = string_of_file.split('\t')
            del_items(l, index)

            if l[3] not in inclusion:
                continue

            if column == 0:

                for st in constant_part:  # добавляет шапку к таблице измерений

                    for cell in st:
                        sheet1.write(row, column, cell, style1)
                        row += 1
                    column += 1
                    row -= len(st)

            i = 0
            for cell in l:      # применяет стиль в зависимости от типа ячейки

                if i < 3:
                    sheet1.write(row, column, cell, style1)
                elif cell == 'FAIL':
                    sheet1.write(row, column, cell, style4)
                elif cell == 'PASS':
                    sheet1.write(row, column, cell, style5)
                elif cell == 'RETRY':
                    sheet1.write(row, column, cell, style6)
                elif cell == '-999' and l[3] == 'FAIL':
                    sheet1.write(row, column, float(cell), style4)
                else:

                    try:

                        cell = float(cell.replace(',', '.'))

                        if limits[i-4][0] <= cell <= limits[i-4][1]:  # проверяет значение
                            sheet1.write(row, column, cell, style)    # ячейки на вхождение
                                                                      # в заданный диапазон
                        else:
                            sheet1.write(row, column, cell, style4)

                    except ValueError:
                        sheet1.write(row, column, '', style6)

                i += 1
                row += 1
            column += 1

            if column == 8:             # задаёт количество столбцов в таблице
                column = 0
                row += len(l)+1

            row -= len(l)

        userName = user_name

        str1 = 'Измерения провёл:'
        str2 = post+'{0}____________{0}'

        if column == 0:
            sheet1.write_merge(row, row, 0, 6,  str1, style3)
            sheet1.write_merge(row+1, row+1, 0, 6,
                               str2.format(' '*16) + userName, style3)

        else:
            sheet1.write_merge(row+len(l)+1, row+len(l)+1, 0, 6, str1, style3)
            sheet1.write_merge(row+len(l)+2, row+len(l)+2, 0, 6,
                               str2.format(' '*16) + userName, style3)

    row = title_maker()
    row, limits = measure_maker(row)
    constant_part, index = constant_part_maker()
    rerult_measurements(row, constant_part, index)

    sheet1.preview_magn = 110     # Значение масштаба при страничном просмотре
    sheet1.page_preview = True    # Страничный просмотр 'Вкл'

    #sheet1.normal_magn = 110     # Значение масштаба при нормальном просмотре
    #sheet1.page_preview = False     # Страничный просмотр 'Выкл'

    if xls_report_name != '':
        if xls_report_name.endswith('.xls'):
            name_file = xls_report_name
        else:
            name_file = xls_report_name + '.xls'

    sheet1.insert_bitmap('image\proton-electrotex.bmp', 0, 6)
    book.save(outcatalog+'\\'+name_file[:-3]+name_file[-3:].replace('txt', 'xls'))
