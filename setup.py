from cx_Freeze import setup, Executable

build_exe_options = {'include_files': ['reports_xls', 'reports_txt', 'image',
                                       'Preferences', 'SaveData'],
                     'includes': ['dbm.gnu', 'dbm.dumb'],
                     'icon': 'image/13.ico'}

setup(
    name="Report_Generator",
    version="0.1",
    description="Report_Generator",
    options={"build_exe": build_exe_options},
    executables=[Executable("Report_Generator.py", base='Win32GUI')])
