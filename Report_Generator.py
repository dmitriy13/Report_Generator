from tkinter import *
from tkinter import ttk
import os
import webbrowser
import shelve
import time
from PIL.ImageTk import PhotoImage
from Report_Creator import xls_crearor
from tkinter.messagebox import *
from tkinter.filedialog import askdirectory, askopenfilename
from shutil import copyfile


########################################################################
# functions
########################################################################

def add_file():

    name_file = askopenfilename(title='Добавить',
                                filetypes=[('Text file', '*txt')])

    if name_file:
        if os.path.split(name_file)[1] not in os.listdir(m_f.input.catalog.get()):
            copyfile(name_file, os.path.join(m_f.input.catalog.get(),
                                             os.path.split(name_file)[1]))
        else:
            showerror('Error', 'File exist already!')

########################################################################
# classes
########################################################################


class AutoScrollbar(ttk.Scrollbar):
    # a scrollbar that hides itself if it's not needed.  only
    # works if you use the grid geometry manager.
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        Scrollbar.set(self, lo, hi)

    def pack(self, **kw):
        raise TclError

    def place(self, **kw):
        raise TclError


class MenuBar():

    '''Создаёт меню окна'''

    def __init__(self, parent):

        self.parent = parent
        self.MakeMenuBar()

    def MakeMenuBar(self):

        self.menubar = Menu(self.parent)
        self.parent.config(menu=self.menubar)
        self.fileMenu()
        self.settingMenu()

    def fileMenu(self):

        pulldown = Menu(self.menubar, tearoff=False)
        pulldown.add_command(label='Add file', command=add_file)
        pulldown.add_command(label='Quit',    command=root.destroy)
        self.menubar.add_cascade(label='File', underline=0, menu=pulldown)

    def settingMenu(self):

        pulldown = Menu(self.menubar, tearoff=False)
        pulldown.add_command(label='Preferences', command=Preferences)
        self.menubar.add_cascade(label='Setting', underline=0, menu=pulldown)


class Preferences(Toplevel):

    def __init__(self):

        Toplevel.__init__(self, padx=10, pady=10)
        self.title('Preferences')

        self.geometry('600x210'+'+'+str(int((root.winfo_width()-600)/2)
                                        + root.winfo_x())
                      + '+'+str(int((root.winfo_height()-200)/3)
                                + root.winfo_y()))

        self.resizable(False, False)
        self.makeWidgets()
        self.focus()
        self.iconbitmap(bitmap='image/13.ico')
        self.focus_set()
        self.grab_set()

    def makeWidgets(self):

        change_catalogs = LabelFrame(self, text='Change catalogs')
        change_catalogs.pack(expand=YES, fill=BOTH, padx=2)

        self.entries = []

        catalog = shelve.open('Preferences\\catalog')

        for item, field in enumerate([('Input catalog', 'reports_txt'),
                                      ('Output catalog', 'reports_xls')]):

            row = LabelFrame(change_catalogs, text=field[0])
            ent = ttk.Entry(row, width=70)
            row.pack(side=TOP, fill=X, pady=3, padx=3)
            ent.pack(side=LEFT, expand=YES, fill=X, pady=5, padx=5)

            try:
                ent.insert(0, catalog[field[1]])
            except KeyError:
                ent.insert(0, os.getcwd()+'\\'+field[1])

            ttk.Button(row, text='Change',
                       command=lambda item=item: self.change_catalog(
                           item)).pack(side=LEFT, padx=3)

            self.entries.append(ent)

        for text, call in (('Ok', lambda: self.seveCatalog(ok=True)),
                           ('Apply', self.seveCatalog),
                           ('Cencel', self.destroy)):

            ttk.Button(change_catalogs,
                       text=text, command=call).pack(side=RIGHT, padx=5)
        catalog.close()

    def change_catalog(self, item):

        pathCatalog = askdirectory().replace('/', '\\')
        if pathCatalog != '':
            self.entries[item].delete(0, END)
            self.entries[item].insert(0, pathCatalog)
        self.entries[item].focus()

    def seveCatalog(self, ok=False):

        catalog = shelve.open('Preferences\\catalog')

        for item, nameShelve in enumerate(['reports_txt', 'reports_xls']):
            if os.path.isdir(self.entries[item].get()):
                catalog[nameShelve] = self.entries[item].get()
                if item == 0:
                    m_f.input.catalog.set(self.entries[item].get())
                elif item == 1:
                    m_f.output.catalog.set(self.entries[item].get())
            else:
                showerror('Error', 'Directoty not exists!')
                self.entries[item].focus()
                return

        catalog.close()

        if ok:
            self.destroy()


class ScrolledText(Frame):

    def __init__(self):

        Frame.__init__(self)
        self.pack(expand=YES, fill=BOTH, padx=7, pady=10)
        self.makeWidgets()

    def makeWidgets(self):

        sbar_y = AutoScrollbar(self)
        sbar_x = AutoScrollbar(self, orient='horizontal')
        text = Text(self, relief=SUNKEN, wrap='none', height=5)
        sbar_y.config(command=text.yview)
        sbar_x.config(command=text.xview)
        text.config(yscrollcommand=sbar_y.set)
        text.config(xscrollcommand=sbar_x.set)
        sbar_y.grid(row=0, column=1, sticky=NSEW)
        sbar_x.grid(row=1, column=0, sticky=NSEW)
        text.grid(row=0, column=0, sticky=NSEW)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.text = text

    def settext(self, file):

        text = open(file, 'r').read()
        self.text.config(state=NORMAL)
        self.text.delete('1.0', END)
        self.text.insert('1.0', text)
        self.text.mark_set(INSERT, '1.0')
        self.text.config(state=DISABLED)


class ProtonImage():

    '''Создаёт изображение в верхней части окна, при нажатии на которое
    открвается браузер с сайтом Протон-Электротекс'''

    def __init__(self, parent=None):

        self.fileName = os.path.join(os.getcwd(), 'image\Proton.jpg')
        self.img = PhotoImage(file=self.fileName)
        self.url = 'http://www.proton-electrotex.com/eng/product'
        lb = Label(parent, image=self.img, relief=SOLID, cursor='hand2')
        lb.pack(side=TOP)
        lb.bind('<Double-1>', self.start_webbrowser)

    def start_webbrowser(self, ignore):
        webbrowser.open(self.url)


class TreeviewList(LabelFrame):

    def __init__(self, parent, text, find_dir, expansion):

        LabelFrame.__init__(self, parent, text=text)
        self.pack(side=LEFT, padx=3, pady=3, expand=YES, fill=BOTH)

        self.catalog = StringVar()
        self.expansion = expansion
        self.list_files = []
        self.find_dir = find_dir
        self.defoult_catalog = os.path.join(os.getcwd(), self.find_dir)
        self.index = 0
        self.reverse = False

        self.catalogShelve = shelve.open('Preferences\\catalog')
        try:
            self.catalog.set(self.catalogShelve[find_dir])
        except KeyError:
            self.catalog.set(self.defoult_catalog)

        self.catalogShelve.close()
        del self.catalogShelve

        self.makeWidgets()
        self.update()

    def makeWidgets(self):

        self.lbl = ttk.Label(self, textvariable=self.catalog, width=10)
        self.lbl.grid(row=0, column=0, sticky=EW, pady=2, columnspan=2)
        sbar_y = AutoScrollbar(self)
        sbar_x = AutoScrollbar(self, orient='horizontal')
        tree = ttk.Treeview(self, selectmode='browse')
        sbar_y.config(command=tree.yview)
        sbar_x.config(command=tree.xview)
        tree.config(yscrollcommand=sbar_y.set, xscrollcommand=sbar_x.set)

        tree['columns'] = ('data')
        tree.column('data', width=1,   anchor='center')
        tree.heading('data', text='Data',
                     command=lambda: self.type_sort('Data'))
        tree.column('#0', width=1,  anchor='center')
        tree.heading('#0', text='Name \u02c4',
                     command=lambda: self.type_sort('Name'))
        tree.tag_bind(0, '<Double-1>', self.runCommand)
        tree.tag_bind(1, '<Double-1>', self.runCommand)
        tree.tag_configure(0,  background='#E6E6FA')
        sbar_y.grid(row=1, column=1, sticky=NSEW)
        sbar_x.grid(row=2, column=0, sticky=NSEW)
        tree.grid(row=1, column=0, sticky=NSEW, pady=3, padx=3)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)

        self.tree = tree

    def type_sort(self, column):

        if column == 'Name':
            self.tree.heading('data', text='Data')
            self.index = 0
            if self.reverse:
                self.reverse = False
                self.tree.heading('#0', text='Name \u02c4')
            else:
                self.reverse = True
                self.tree.heading('#0', text='Name \u02c5')

        elif column == 'Data':
            self.tree.heading('#0', text='Name')
            self.index = 1
            if self.reverse:
                self.reverse = False
                self.tree.heading('data', text='Data \u02c4')
            else:
                self.reverse = True
                self.tree.heading('data', text='Data \u02c5')

        self.sort_insert(True)

    def update(self):

        if not os.path.isdir(self.catalog.get()):
            self.catalog.set(self.defoult_catalog)

        list_files = os.listdir(self.catalog.get())
        list_files = list(filter(lambda x: x.endswith(self.expansion),
                                 list_files))
        list_files = list(map(lambda x: (x, os.path.getmtime(
            os.path.join(self.catalog.get(), x))), list_files))
        list_files.sort(key=lambda x: x[0].lower())

        if list_files != self.list_files:

            self.tree.delete(*self.list_files)
            self.list_files = list_files
            self.sort_insert()

        self.after(2000, self.update)

    def sort_insert(self, del_ok=False):

        if del_ok:
            self.tree.delete(*self.list_files)
        for tag, label in enumerate(sorted(self.list_files,
                                           key=lambda x: x[self.index],
                                           reverse=self.reverse)):
            value = time.strftime('%d.%m.%Y %X', time.localtime(label[1]))
            self.tree.insert('', 'end', label, text=label[0], values=(value,)
                             , tags=tag % 2)

    def runCommand(self, event):
        assert False, 'Must be redefine in subclass'


class InputTreeviewList(TreeviewList):

    def __init__(self,  parent, text, find_dir, expansion):

        TreeviewList.__init__(self,  parent, text, find_dir, expansion)

    def runCommand(self, event):

        s_t.settext(self.catalog.get()+'\\'+self.tree.item(
            self.tree.selection()[0], 'text'))


class OutputTreeviewList(TreeviewList):

    def __init__(self, parent, text, find_dir, expansion):

        TreeviewList.__init__(self, parent, text, find_dir, expansion)

    def runCommand(self, event):

        os.startfile(os.path.join(self.catalog.get(),
                                  self.tree.item(self.tree.selection()[0],
                                                 'text')).replace('/', '\\'))


class DataField(Frame):

    def __init__(self, parent):

        ttk.Frame.__init__(self, parent)
        self.pack(side=LEFT, padx=5, pady=2)
        fields = ['xls name (if empty, name is txt name)',
                  'Post (may be empty)',
                  'Surname, name, patronymic *',
                  'Notation (may be empty)']
        fields_combobox = ['Post (may be empty)',
                           'Surname, name, patronymic *']
        self.data_field = self.makeform(fields, fields_combobox)

    def makeform(self, fields, fields_combobox):

        SaveData = shelve.open('SaveData\\lists')

        entries = []
        for field in fields:
            row = LabelFrame(self, text=field, pady=3, padx=1)
            if field in fields_combobox:
                ent = ttk.Combobox(row, height=7, width=13)
                if field in SaveData:
                    data = SaveData[field]
                    if field == 'Surname, name, patronymic *' and 'Макарин Д. Г.' not in data:
                        data.append('Макарин Д. Г.')
                        SaveData[field] = data
                    ent.config(values=data)
                ttk.Button(row, text='del ', width=4,
                           command=lambda field=field: self.del_note(
                               field)).pack(side=RIGHT, padx=2)
                ttk.Button(row, text='add', width=4,
                           command=lambda field=field: self.add_note(
                               field)).pack(side=RIGHT, padx=2)
            else:
                ent = ttk.Entry(row, width=35)
                if field == 'xls name (if empty, name is txt name)':
                    ttk.Button(row, text='+', width=3,
                               command=self.create_name_of_file).pack(
                                   side=RIGHT, padx=2)

            row.pack(side=TOP, fill=X, pady=3)
            ent.pack(side=BOTTOM, expand=YES, fill=X)
            entries.append(ent)

        SaveData.close()

        radio_include_report = LabelFrame(self, text='Modules included in report')
        radio_include_report.pack(expand=YES, fill=BOTH)

        self.v = StringVar()

        ttk.Radiobutton(radio_include_report, text="Pass", variable=self.v,
                        value='PASS').pack(side=LEFT, expand=YES,
                                           fill=BOTH)

        ttk.Radiobutton(radio_include_report, text="Fail", variable=self.v,
                        value='FAIL').pack(side=LEFT, expand=YES,
                                           fill=BOTH)

        ttk.Radiobutton(radio_include_report, text="Both", variable=self.v,
                        value='PASS, FAIL').pack(side=LEFT, expand=YES,
                                                 fill=BOTH)

        self.v.set('PASS, FAIL')

        return entries

    def create_name_of_file(self):

        try:
            try:
                file = open(os.path.join(m_f.input.catalog.get(),
                                         m_f.input.tree.item(
                                             m_f.input.tree.selection()[0],
                                             'text')),
                            'r', encoding='windows-1251')
            except IndexError:
                showerror('Error', 'Choose item in "Input catalog"!')
                return

            for string_of_file in file:
                l = string_of_file.replace('\n', '').split('\t')
                x = l[1].split('-')
                type_of_module = x[0][-2:]
                name_of_modul = ''.join(x[1:])
                break

            i = 0
            for string_of_file in file:
                if len(string_of_file) == 1:
                    i += 1
                    if i == 2:
                        break

            i = 0
            for string_of_file in file:
                if i == 2:
                    l = string_of_file.split('\t')
                    x = l[6].split('.')
                    x.reverse()
                    deta_of_measure = ''.join(x)[2:]
                    break
                i += 1

            name_of_file = '{0}_{1}_STAT_{2}_{3}'.format(
                deta_of_measure, type_of_module, name_of_modul,
                m_f.input.tree.item(m_f.input.tree.selection()[0],
                                    'text')[:-4])

            self.data_field[0].delete(0, END)
            self.data_field[0].insert(0, name_of_file)

        except :
                showerror('Error', 'Cannot create name!')
                return

    def del_note(self, field):

        if field == 'Post (may be empty)':
            index = 1
        elif field == 'Surname, name, patronymic *':
            index = 2

        SaveData = shelve.open('SaveData\\lists')
        if field in SaveData:
            data = SaveData[field]

            if not (index == 2 and self.data_field[index].get() == 'Макарин Д. Г.'):
                try:
                    data.remove(self.data_field[index].get())
                    self.data_field[index].config(values=data)
                except ValueError:
                    showerror('Error', 'Not exists!')

                SaveData[field] = data

        SaveData.close()

    def add_note(self, field):

        if field == 'Post (may be empty)':
            index = 1
        elif field == 'Surname, name, patronymic *':
            index = 2

        SaveData = shelve.open('SaveData\\lists')
        if field not in SaveData:
            SaveData[field] = []

        data = SaveData[field]

        if self.data_field[index].get() not in data and self.data_field[index].get() !='':
            data.append(self.data_field[index].get())
            data.sort()
            self.data_field[index].config(values=data)

            SaveData[field] = data
        SaveData.close()


class MainFrame(LabelFrame):

    def __init__(self, parent=None):

        Frame.__init__(self, parent)
        self.pack(side=TOP, fill=BOTH, pady=3)
        self.makeWidgets()

    def makeWidgets(self):

        self.input = InputTreeviewList(self, 'Input catalog',
                                       'reports_txt', 'txt')
        self.field = DataField(self)
        ttk.Button(self, text='Generate', command=self.create_xls).pack(
            side=LEFT, padx=7, pady=105, fill=Y)
        self.output = OutputTreeviewList(self, 'Output catalog',
                                         'reports_xls', 'xls')

    def create_xls(self):

        if not self.field.data_field[2].get():
            showerror('Error', ' "Surname, name, patronymic" field is empty! ')
        else:

            try:
                name_file = self.input.tree.item(
                    self.input.tree.selection()[0], 'text')
            except IndexError:
                showerror('Error', 'Choose item in "Input catalog"!')
                return

            xls_report_name = self.field.data_field[0].get()
            post = self.field.data_field[1].get()
            user_name = self.field.data_field[2].get()
            notation = self.field.data_field[3].get()
            inclusion = self.field.v.get()

            args = (name_file, xls_report_name, post, user_name,
                    notation, inclusion, self.input.catalog.get(),
                    self.output.catalog.get())

            try:
                xls_crearor(*args)
            except Exception as err:
                showerror('Error', 'Report is not created!\nReason: '+str(err))
            else:
                showinfo('Reference', 'Report is created.')


if __name__ == "__main__":
    root = Tk()
    x = root.winfo_screenwidth()
    y = root.winfo_screenheight()

    root.geometry('1000x600+'+str(int((x-1000)/2))+'+'+str(int((y-600)/3)))
    root.title('IGBT report generator')
    root.minsize(width=850, height=475)

    MenuBar(root)
    p_i = ProtonImage(root)
    m_f = MainFrame(root)
    s_t = ScrolledText()
    root.iconbitmap(bitmap='image/13.ico')

    root.mainloop()
